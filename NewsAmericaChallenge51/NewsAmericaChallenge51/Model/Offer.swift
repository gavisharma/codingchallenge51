//
//  Offer.swift
//  NewsAmericaChallenge51
//
//  Created by Govinda Sharma on 2019-12-07.
//  Copyright © 2019 Govinda Sharma. All rights reserved.
//

import UIKit
import Foundation

class Offer: NSObject {
  private var id: Int = 0
  private var name: String = ""
  private var imageURL: String = ""
  private var cashBack: Double = 0.0
  
  init(_ offer: [String: Any]) {
    if let objId = offer[kKey_OfferId] as? Int {
      id = objId
    }
    
    if let objName = offer[kKey_OfferName] as? String {
      name = objName
    }

    if let objImageUrl = offer[kKey_OfferImageUrl] as? String {
      imageURL = objImageUrl
    }
    
    if let objCashBack = offer[kKey_OfferCashback] as? Double {
      cashBack = objCashBack
    }
    
  }
  
  var offerId: Int {
    get { return id }
    set { id = newValue }
  }
  
  var offerName: String {
    get { return name }
    set { name = newValue }
  }
  
  var offerImageURL: String {
    get { return imageURL }
    set { imageURL = newValue }
  }
  
  var offerCashBack: Double {
    get { return cashBack }
    set { cashBack = newValue }
  }
  
}
