//
//  Constants.swift
//  NewsAmericaChallenge51
//
//  Created by Govinda Sharma on 2019-12-07.
//  Copyright © 2019 Govinda Sharma. All rights reserved.
//

import Foundation

let kKey_FileName       = "c51"
let kKey_FileExtension  = "json"

let kKey_DataKey        = "offers"

let kKey_OfferId        = "offer_id"
let kKey_OfferName      = "name"
let kKey_OfferImageUrl  = "image_url"
let kKey_OfferCashback  = "cash_back"

let kKey_CellIdentifier = "OfferCell"
