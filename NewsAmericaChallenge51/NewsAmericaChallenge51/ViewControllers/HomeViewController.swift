//
//  ViewController.swift
//  NewsAmericaChallenge51
//
//  Created by Govinda Sharma on 2019-12-06.
//  Copyright © 2019 Govinda Sharma. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

  @IBOutlet private weak var offersTableView: UITableView!
  var fileManager = FileManager.default
  var offerList: [Offer] = [Offer]()
  
  enum Sort {
    case Name
    case Cashback
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    guard let mainUrl = Bundle.main.url(forResource: kKey_FileName, withExtension: kKey_FileExtension) else { return }
    readData(pathName: mainUrl)
  }
  
  func readData(pathName: URL){
    do {
      let jsonData = try Data(contentsOf: pathName)
      let json = try JSONSerialization.jsonObject(with: jsonData) as! [String: Any]
      let dataArray = json[kKey_DataKey] as! [[String: Any]]
      for offerObject in dataArray {
        let offer: Offer = Offer(offerObject)
        offerList.append(offer)
      }
      offersTableView.reloadData()
    }
    catch {
      print(error)
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return offerList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: kKey_CellIdentifier) as! OfferTableViewCell
    cell.configureCellWith(offer: offerList[indexPath.row])
    cell.selectionStyle = .none
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80.0
  }
  
  @IBAction func sortOffersAction(_ sender: Any) {
    let segmentedControl = sender as! UISegmentedControl
    segmentedControl.selectedSegmentIndex == 0 ? sort(with: Sort.Name) : sort(with: Sort.Cashback)
  }
  
  
  func sort(with property: Sort) {
    switch property {
    case .Name:
      offerList.sort(by: { $0.offerName > $1.offerName })
    case .Cashback:
      offerList.sort(by: { $0.offerCashBack > $1.offerCashBack })
    }
    offersTableView.reloadData()
  }

}

