//
//  OfferTableViewCell.swift
//  NewsAmericaChallenge51
//
//  Created by Govinda Sharma on 2019-12-07.
//  Copyright © 2019 Govinda Sharma. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {

  @IBOutlet weak var offerNameLabel: UILabel!
  @IBOutlet weak var offerCashBackLabel: UILabel!
  @IBOutlet weak var offerImageView: UIImageView!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
  }
  
  func setupImageView() {
    offerImageView.layer.cornerRadius = offerImageView.frame.size.height/2
    offerImageView.clipsToBounds = true
    offerImageView.layer.borderWidth = 1.0
    offerImageView.layer.borderColor = UIColor.blue.cgColor
  }
  
  func configureCellWith(offer: Offer) {
    offerNameLabel.text = offer.offerName
    offerCashBackLabel.text = "Cashback: $\(offer.offerCashBack)"
    if let url = URL(string:offer.offerImageURL){
      getData(from: url) { (data, response, error) in
        if (error == nil){
          DispatchQueue.main.async() {
            self.offerImageView.image = UIImage(data: data!)
            self.setupImageView()
          }
        }
      }
    }
  }
  
  //MARK: Downloads the images
  func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
    URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
  }

}
